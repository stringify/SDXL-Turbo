import gradio as gr
import torch
import numpy as np
from diffusers import DiffusionPipeline 
import subprocess

device = "cuda" if torch.cuda.is_available() else "cpu"
import os

G_TOKEN = os.getenv('token')

ret, out = subprocess.getstatusoutput(f'cd /home && git -c lfs.url="http://lfs-internal.gitee.com/hf-models/sdxl-turbo.git/info/lfs" clone https://oauth2:{G_TOKEN}@gitee.com/hf-models/sdxl-turbo.git  --depth=1 --single-branch --progress --verbose')
print("G_TOKEN:", G_TOKEN)
print("ret:", ret)
print("out:", out)

if torch.cuda.is_available():
    print("cuda")
    torch.cuda.max_memory_allocated(device=device)
    torch.cuda.empty_cache()
    pipe = DiffusionPipeline.from_pretrained("/home/sdxl-turbo", torch_dtype=torch.float16, variant="fp16", use_safetensors=True)
    pipe.enable_xformers_memory_efficient_attention()
    pipe = pipe.to(device)
    torch.cuda.empty_cache()
else: 
    print("cpu")
    pipe = DiffusionPipeline.from_pretrained("/home/sdxl-turbo", use_safetensors=True)
    pipe = pipe.to(device)
    
def genie (prompt, steps, seed, scale, image_size):
    generator = np.random.seed(0) if seed == 0 else torch.manual_seed(seed)
    int_image = pipe(prompt=prompt,width=image_size,height=image_size,generator=generator, num_inference_steps=steps, guidance_scale=scale).images[0]
    return int_image
    
grModel = gr.Interface(fn=genie, inputs=[gr.Textbox(label='最多 77 Token'), 
    gr.Slider(1, maximum=30, value=2, step=1, label='迭代次数'), 
    gr.Slider(minimum=0, step=1, maximum=999999999999999999, randomize=True),
    gr.Slider(minimum=0, step=1, maximum=3, label='引导比例'),
    gr.Slider(minimum=512, step=1, maximum=1920, label='分辨率'),
    ],
    outputs='image', 
    examples=[["cat wizard, gandalf, lord of the rings, detailed, fantasy, cute, adorable, Pixar, Disney, 8k"], ["PRETTY CUTE GIRL BY ROSSDRAWS. An extradimensional creature buying donuts. Pixar animation."]],
    title="Gitee AI - Stable Diffusion Turbo", 
    allow_flagging="never",
    description="SDXL Turbo. <br><br><b>WARNING: This model is capable of producing NSFW (Softcore) images.</b>", 
    article = "Hosted on gitee-ai")

grModel.queue(2)

grModel.launch(debug=True, max_threads=80)
